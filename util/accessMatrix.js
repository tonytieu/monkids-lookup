export default [
    {
        role: 'nea',
        links: [
            '/',
            "/tenders",
            "/settings",
            "/search",
            "/tenders/award-tender",
        ]
    },
    {
        role: 'mop',
        links: [
            '/',
            "/tenders/completed",
            "/tenders/pending",
            "/tenders/find",
            "/tenders/agreement-signing",
            "/search"
        ]
    },
]