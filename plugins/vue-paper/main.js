import Vue from 'vue'
import './pollyfills'
import Notify from 'vue-notifyjs'
import VeeValidate from 'vee-validate'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import SideBar from '~/components/UIComponents/SidebarPlugin'
// library imports

import '~/assets/sass/paper-dashboard.scss'
import '~/assets/sass/demo.scss'

import sidebarLinks from './sidebarLinks'
import './registerServiceWorker'
// plugin setup
// Vue.use(VueRouterPrefetch)
Vue.use(GlobalDirectives)
Vue.use(GlobalComponents)
Vue.use(Notify)
console.log('vue notify', Notify);

Vue.use(SideBar, {sidebarLinks: sidebarLinks})
Vue.use(VeeValidate)
locale.use(lang)

Vue.mixin({
    computed: {
    },
    methods: {
      isAuthorized(path) {
        console.log(path);
        
      },
    }
});

import accessMatrix from '~/util/accessMatrix'

Vue.directive('isAuthorized', {
    inserted(el, binding, vnode, old) {
      //   let path = el.querySelector('a').getAttribute('href')
      //   let user = JSON.parse(sessionStorage.getItem('user'))
      //   //console.log(el, el.querySelector('a').getAttribute('href'));
        
      // if (accessMatrix.find(x => x.role == user.userName).links.findIndex(x => x == path) == -1) {
      //   vnode.elm.parentElement.removeChild(vnode.elm)
      // }
    }
})